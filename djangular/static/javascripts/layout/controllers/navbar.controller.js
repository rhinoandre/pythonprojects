app.controller('NavbarController', ['$scope', 'Authentication',
    function($scope, Authentication){
        $scope.logout = function(){
            Authentication.logout();
        }
    }]);