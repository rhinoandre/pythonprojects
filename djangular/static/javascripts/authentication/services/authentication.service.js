app.factory('Authentication', ['$cookies', '$http',
    function($cookies, $http){
        var service = {};
        service.register = function(email, username, password){
            return $http.post('/api/v1/accounts/', {
                username: username,
                email: email,
                password: password
            }).success(function(data, status, header, config){

                service.login(email, password);

            }).error(function(data, status, header, config){

                console.error('Epic Failure!');

            });
        }

        service.login = function(email, password){
            return $http.post('/api/v1/auth/login/', {
                email: email, password: password
            }).success(function(data, status, header, config){

                service.setAuthenticatedAccount(data.data);
                window.location = '/';

            }).error(function(data, status, header, config){

                console.error('Epic Failure!');

            });
        }

        service.logout = function(){
            return $http.post('api/v1/auth/logout/').
                success(function(data, status, header, config){

                    service.unauthenticate();
                    window.location = '/';

                }).error(function(data, status, header, config){
                    console.error('Epic failure!');
                })
        };

        /**
         * @name getAuthenticatedAccount
         * @desc Return the currently authenticated account
         * @returns {object|undefined} Account if authenticated, else `undefined`
         * @memberOf djangular.authentication.services.Authentication
         */
        service.getAuthenticationAccount = function(){
            if(!$cookies.authenticatedAccount){
                return;
            }
            return JSON.parse($cookies.authenticatedAccount);
        }

        /**
         * @name isAuthenticated
         * @desc Check if the current user is authenticated
         * @returns {boolean} True is user is authenticated, else false.
         * @memberOf djangular.authentication.services.Authentication
         */
        service.isAuthenticated = function(){
            return !!$cookies.authenticatedAccoun;
        }

        /**
         * @name setAuthenticatedAccount
         * @desc Stringify the account object and store it in a cookie
         * @param {Object} user The account object to be stored
         * @returns {undefined}
         * @memberOf djangular.authentication.services.Authentication
         */
        service.setAuthenticatedAccount = function(account){
            $cookies.authenticatedAccount = JSON.stringify(account);
        }
        /**
         * @name unauthenticate
         * @desc Delete the cookie where the user object is stored
         * @returns {undefined}
         * @memberOf djangular.authentication.services.Authentication
         */
        service.unauthenticate = function(){
            delete $cookies.authenticatedAccount;
        }

        return service;
    }
]);
