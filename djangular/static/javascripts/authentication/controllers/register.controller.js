app.controller('RegisterController', ['$location', '$scope','Authentication',
    function($location, $scope, Authentication){

        if(Authentication.isAuthenticated()){
                $location.url("/");
        }

        $scope.register = function(){
            //TODO Remove the callback from the register method
            Authentication.register($scope.email, $scope.username, $scope.password);
        }
    }]
);