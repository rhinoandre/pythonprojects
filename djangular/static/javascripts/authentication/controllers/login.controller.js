app.controller('LoginController', ['$location', '$scope', 'Authentication',
    function($location, $scope, Authentication){

        if(Authentication.isAuthenticated()){
                $location.url("/");
        }

        $scope.login = function(){
            Authentication.login($scope.email, $scope.password);
        }

    }]);