var app = angular.module('djangularApp', ['ngRoute', 'ngCookies']);

app.config(function($locationProvider){
        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('!');
});

app.config(function($routeProvider){
        $routeProvider.
            when('/register', {
                controller: 'RegisterController',
                templateUrl: '/static/templates/authentication/register.html'
            }).when('/login', {
                controller:'LoginController',
                templateUrl: '/static/templates/authentication/login.html'
            }).
            otherwise('/');
    });

app.run(['$http', function($http){
    $http.defaults.xsrfHeaderName = 'X-CSRFToken';
    $http.defaults.xsrfCookieName = 'csrftoken';
}]);